## Synopsis

Metabuild is a repository manager for the PiBox Media System, PiBox Kiosk and Auto-Kiosk, and Xeon projects.  It builds sets of applications associated with these projects based on a specified PiBox Development Platform build, tags application repositories and prepares changelogs for PiBox releases.  

Application lists are stored in src/pkglist.project.  These files lists what applications are associated with each project.  The default build is for the PiBox Media System which uses the src/pkglist.txt list.

Each application has an associated cdtools setup function stored in a shell script under src/env.  These scripts are sourced by the top level build.sh in order to identify how to download and build the application.

The cross-toolchain and root file system staging tree must be available locally and are pointed at with command line options to the build.sh script.

## Installation

Metabuild is based on GNU Make.  It designed to build application sets for whatever hardware the designated PiBox Development Platform has been targeted.

Before proceeding, read any Linux distribution-specific notes, such as "fedora.notes".  If there are none, then don't worry about it.

The build depends on the cdtools environment setup.  The environment file is in scripts/meta.sh.  See [cdtools](http://www.graphics-muse.org/wiki/pmwiki.php/Cdtools/Cdtools) documentation for it's use.

After editing meta.sh, source the script:

    . cdtools

Then setup your environment by running the function found in the meta.sh script:

    meta

Now you're ready to retrieve the source.  Run the following command to see how to clone the source tree:

    cd?

This will tell you exactly what to do.  After you clone the source you can do perform package builds with the build.sh script.  To get help, use the following command.

    ./build.sh -?

To generate a release changelog, including tagging repositories, use the MakeChangelong.sh script.  To get help for tagging and changelog generatation use the following command.

    ./MakeChangelog.sh -?

## Contributors

To get involved with PiBox, contact the project administrator:
Michael J. Hammel <mjhammel@graphics-muse.org>

## License

0BSD 
