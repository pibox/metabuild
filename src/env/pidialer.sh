#!/bin/bash -p
# Functions and aliases used to nagivate project directories.
# Include by the ~/bin/cdtools script.
#############################################################################

# -------------------------------------------------------------------
# DESC: pidialer - GTK+ dialing application for PiBox
# Edit variables with <> values, as needed.
# -------------------------------------------------------------------
function pidialer {
    # -------------------------------------------------------------------
    # Edit these items
    # -------------------------------------------------------------------

    # Top of the source tree
    # The project directory will be placed under SRCTOP
    SRCTOP=$METABLDDIR

    # -------------------------------------------------------------------
    # Don't edit below here
    # -------------------------------------------------------------------

    # Project name
	PARENT=pibox
    PRJ=pidialer

    # If supplied, the second argument is a suffix, to allow multiple versions of the same tree.
    SFX=$1

    # Repository
    export GITREPO=git@gitlab.com:$PARENT/$PRJ.git
    GM_CLONE="git clone $GITREPO $METABLDDIR/$PRJ"

    # Create top level directory, if needed
    mkdir -p $SRCTOP

    # Where I do my dev work
    GM_WORK=$SRCTOP/work
    mkdir -p $GM_WORK

    # Where the SCM is located
    GM_HOME=$SRCTOP/$PRJ$SFX

    # Where the source and build directories live
    GM_SRC=$GM_HOME/src
    GM_BUILD=$GM_HOME/bld

    # Archives, packaging extras
    GM_ARCHIVE=$GM_HOME/archive
    GM_PKG=$GM_HOME/opkg
    GM_EXTRAS=$GM_HOME/extras

    # Make the configured environment available 
    export GM_WORK
    export GM_ARCHIVE
    export GM_HOME
    export GM_SRC
    export GM_BUILD
    export GM_PKG
    export GM_EXTRAS

    # Some aliases to bounce around directories easily
    alias cdt='cd $SRCTOP'
    alias cdh='cd $GM_HOME'
    alias cdw='cd $GM_WORK'
    alias cdx='cd $GM_SRC'
    alias cdb='cd $GM_BUILD'
    alias cdp='cd $GM_PKG'
    alias cda='cd $GM_ARCHIVE'
    alias cde='cd $GM_EXTRAS'

    # Show the aliases for this configuration
    alias cd?='listpidialer'
}
function listpidialer {
echo "
$PRJ Alias settings:
-----------------------------------------------------------------------------
cdt    cd SRCTOP ($SRCTOP)
cdh    cd GM_HOME ($GM_HOME)
cdw    cd GM_WORK ($GM_WORK)
cdx    cd GM_SRC ($GM_SRC)
cdb    cd GM_BUILD ($GM_BUILD)
cdp    cd GM_PKG ($GM_PKG)
cda    cd GM_ARCHIVE ($GM_ARCHIVE)
cde    cd GM_EXTRAS ($GM_EXTRAS)

CLONE: $GM_CLONE
"
}
