#!/bin/bash -p
# Functions and aliases used to nagivate project directories.
# Include by the ~/bin/cdtools script.
#############################################################################

# -------------------------------------------------------------------
# DESC: Ironman projects overlaying PiBox Development Platform
# Edit variables with <> values, as needed.
# -------------------------------------------------------------------
function ironman {

    # If supplied, the first argument is a suffix, to allow multiple versions of the same tree.
    SFX=$1

    # -------------------------------------------------------------------
    # Edit these items
    # -------------------------------------------------------------------

    # Top of the source tree
    # The project directory will be placed under SRCTOP
    # SRCTOP=<PATH TO WHERE PROJECT DIRECTORY WILL LIVE>
    SRCTOP=$METABLDDIR

    # -------------------------------------------------------------------
    # Don't edit below here
    # -------------------------------------------------------------------
    # Project ID
    PARENT=xironman

    # Project ID:  Defaults to "monitor" unless first argument is set
    if [ "$1" != "" ]
    then
        case "$1" in
        "monitor")       PRJ=$1;;
        "gpio")          PRJ=$1;;
        "contacts")      PRJ=$1;;
        "sensors")       PRJ=$1;;
        "ironmancfg")    PRJ=$1;;
        "daemon")        PRJ=$1;;
        "launcher")      PRJ=$1;;
        "www")           PRJ=$1;;
        "imrest")        PRJ=$1;;
        "mobile")        PRJ=$1;;
        "meta")          PRJ=$1;;
        *) 
            echo "Invalid repo"
            ironmanrepos
            return 0
            ;;
        esac
    else
        PRJ=monitor
    fi

    # GIT Repo
    export GITREPO=git@gitlab.com:$PARENT/$PRJ.git
    GM_CLONE="git clone $GITREPO $METABLDDIR/$PRJ"

    # Suffix allows for creating multiple trees for the same repo
    if [ "$1" != "" ]
    then
        SFX=$2
    else
        SFX=$1
    fi

    # Create top level directory, if needed
    mkdir -p $SRCTOP

    # Where I do my dev work
    GM_WORK=$SRCTOP/work
    # Where the SCM is located
    GM_HOME=$SRCTOP/$PRJ$SFX
    # Where the source and build directories live
    GM_SRC=$GM_HOME
    GM_BUILD=$GM_HOME/bld
    GM_ARCHIVE=$GM_HOME/archive
    GM_EXTRAS=$GM_HOME/extras

    case "$PRJ" in
        "monitor")       GM_PKG=$SRCTOP/pkg;;
        "gpio")          GM_PKG=$GM_HOME/opkg;;
        "contacts")      GM_PKG=$SRCTOP/pkg;;
        "sensors")       GM_PKG=$GM_HOME/opkg;;
        "ironmancfg")    GM_PKG=$SRCTOP/pkg;;
        "daemon")        GM_PKG=$SRCTOP/pkg;;
        "launcher")      GM_PKG=$GM_HOME/opkg;;
        "www")           GM_PKG=$SRCTOP/pkg;;
        "imrest")        GM_PKG=$GM_HOME/opkg;;
        "mobile")        GM_PKG=$SRCTOP/pkg;;
        "meta")          GM_PKG=$SRCTOP/pkg;;
    esac

    # Put the host applications in our path
    # export PATH=$GM_TOOLS:$PATH

    # Make the configured environment available to the build system.
    export GM_WORK
    export GM_ARCHIVE
    export GM_PKG
    export GM_HOME
    export GM_SRC
    export GM_BUILD
    export GM_EXTRAS
    export GM_CLONE

    # Some aliases to bounce around directories easily
    alias cdt='cd $SRCTOP'
    alias cdh='cd $GM_HOME'
    alias cdw='cd $GM_WORK'
    alias cdx='cd $GM_SRC'
    alias cdb='cd $GM_BUILD'
    alias cda='cd $GM_ARCHIVE'
    alias cdp='cd $GM_PKG'
    alias cde='cd $GM_EXTRAS'
    alias cdl='ironmanrepos'

    # Show the aliases for this configuration
    alias cd?='cdironman'
}
function cdironman {
echo "
$PARENT $PRJ Alias settings:
-----------------------------------------------------------------------------
cdt    cd SRCTOP ($SRCTOP)
cdh    cd GM_HOME ($GM_HOME)
cdw    cd GM_WORK ($GM_WORK)
cdx    cd GM_SRC ($GM_SRC)
cdb    cd GM_BUILD ($GM_BUILD)
cda    cd GM_ARCHIVE ($GM_ARCHIVE)
cdp    cd GM_PKG ($GM_PKG)
cde    cd GM_EXTRAS ($GM_EXTRAS)
cdl    List available repositories

CLONE: $GM_CLONE

"
}
function ironmanrepos {
echo "
$PRJ repos:
--------------------------------------------------------------------------------
monitor:    Updates to PiBox Development Platform to create Ironman Monitor core
gpio:       GPIO utility for setting and reading GPIO pins on the Raspberry Pi
contacts:   Contact backend - manage lists for sending notifications
sensors:    Sensor backend - Browse and manage sensors and sensor groups from monitor
ironmancfg: Administrative backend - manage admin pw, keys
daemon:     Daemons used in Ironman project
launcher:   Home Automation UI for monitor
www:        Web extensions - wifi/ap, contacts, sensors, files, setup
imrest:     RESTful Web - used for sensors/Jarvis interaction with Ironman
mobile:     Android app - wifi/ap, contacts, sensors, files
meta:       Metabuilder - builds all PiBox add-on packages associated with Ironman for distribution.
"
}



