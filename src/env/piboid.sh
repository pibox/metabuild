#!/bin/bash -p
# Functions and aliases used to nagivate project directories.
# Include by the ~/bin/cdtools script.
#############################################################################

# -------------------------------------------------------------------
# DESC: PiBox Android Remote Control
# -------------------------------------------------------------------
function piboid {

    # If supplied, the second argument is a suffix, to allow multiple versions of the same tree.
    SFX=$1
    
    # -------------------------------------------------------------------
    # Edit these items
    # -------------------------------------------------------------------

    # Top of the source tree
    # The project directory will be placed under SRCTOP
    # SRCTOP=<PATH TO WHERE PROJECT DIRECTORY WILL LIVE>
    SRCTOP=$METABLDDIR

    # -------------------------------------------------------------------
    # Don't edit below here
    # -------------------------------------------------------------------

    # Project name
    PARENT=pibox
    PRJ=piboid

    # Repository
    export GITREPO=git@gitlab.com:$PARENT/$PRJ.git
	GM_CLONE="git clone $GITREPO $METABLDDIR/$PRJ"

    # Where I do my dev work
    GM_WORK=$SRCTOP/work

    # Where the SCM is located
    GM_HOME=$SRCTOP/$PRJ$SFX

    # Where the source and build directories live
    GM_SRC=$GM_HOME/src
    GM_BUILD=$GM_HOME/bld

    # Archives, packaging extras
    GM_ARCHIVE=$GM_HOME/archive
    GM_PKG=$GM_HOME/pkg
    GM_EXTRAS=$GM_HOME/extras

    # Android locations
    GM_ANDROID=$SRCTOP/android
    ANDROID_SDK=$GM_ANDROID/android-sdk-linux
    ANDROID_PLATFORM_TOOLS=$ANDROID_SDK/platform-tools
    ANDROID_TOOLS=$ANDROID_SDK/tools
	ANDROID_HOME=$ANDROID_SDK

    # Where Java is installed
    JAVA_HOME="/usr/java/default"
    JAVA_BIN="$JAVA_HOME/bin"

    # Extra tools
    GM_TOOLS="$ANDROID_TOOLS:$ANDROID_PLATFORM_TOOLS:$JAVA_BIN"

    # Put the Android tools in our path
    export PATH=$GM_TOOLS:$PATH

    # Make the configured environment available to the build system.
    export SRCTOP GM_WORK GM_SRC GM_TOOLS ANDROID_SDK ANDROID_HOME ANDROID_TOOLS ANDROID_PLATFORM_TOOLS JAVA_HOME

    # Some aliases to bounce around directories easily
    alias cdt='cd $SRCTOP'
    alias cdh='cd $GM_HOME'
    alias cdw='cd $GM_WORK'
    alias cdx='cd $GM_SRC'
    alias cdb='cd $GM_BUILD'
    alias cdp='cd $GM_PKG'
    alias cda='cd $GM_ARCHIVE'
    alias cde='cd $GM_EXTRAS'
    alias cdn='cd $GM_ANDROID'
    alias cd?='listpiboid'

    # ----------------------------
    # Android commands aliases
    # ----------------------------

    # Virtual Device Manager (also SDK manager)
    alias adbs='sudo $ANDROID_PLATFORM_TOOLS/adb kill-server && sudo $ANDROID_PLATFORM_TOOLS/adb start-server'
    alias avd='android avd&'
    alias kavd='killall -9 emulator'
	alias ref='firefox http://developer.android.com/reference/packages.html'
    alias alog='adb logcat | less'
    alias alogc='adb logcat -c'

    # List available targets
    alias targets='android list targets'
}

function listpiboid {
echo "
Android Test Application alias settings:
-----------------------------------------------------------------------------
cdt      cd SRCTOP ($SRCTOP)
cdh      cd GM_HOME ($GM_HOME)
cdw      cd GM_WORK ($GM_WORK)
cdx      cd GM_SRC ($GM_SRC)
cdb      cd GM_BUILD ($GM_BUILD)
cdp      cd GM_PKG ($GM_PKG)
cda      cd GM_ARCHIVE ($GM_ARCHIVE)
cde      cd GM_EXTRAS ($GM_EXTRAS)
cdn      cd GM_ANDROID ($GM_ANDROID)
adbs     adb kill-server && adb start-server
avd      android avd
kavd     killall -9 emulator
targets  android list targets
ref      firefox http://developer.android.com/reference/packages.html
alog     adb logcat | less (show the log)
alogc    adb logcat -c (clears the log)

Repository: $REPO
ANDROID_HOME: $ANDROID_HOME

Prerequisites:
    yum install glibc.i686 glibc-devel.i686 libstdc++.i686 zlib-devel.i686 \
    ncurses-devel.i686 libX11-devel.i686 libXrender.i686 libXrandr.i686

To create a new android project:
targets (to get list of android target ids)
android create project --target <target-id> --name <appname> \\
    --path <path-to-workspace>/<appname> --activity <appactivity> \\
    --package com.example.<appname>

To build an app with debug:   ant debug
To push to the emulator:      adb install bin/MyFirstApp-debug.apk
    Note: the emulator must have been started via avd first.
    Note: the device should have developer debug enabled.
    -e forces install to only running emulator
    -d forces install to only attached device
    install -r allows for reinstall of apk

To checkout tree:
cdt
mkdir $PRJ$SFX
cdh
git clone $GITREPO src
"
}

