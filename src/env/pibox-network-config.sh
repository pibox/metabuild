#!/bin/bash -p
# Functions and aliases used to nagivate project directories.
# Include by the ~/bin/cdtools script.
#############################################################################

# -------------------------------------------------------------------
# DESC: PiBox network configuration utility and library
# -------------------------------------------------------------------
function pibox-network-config {
    # -------------------------------------------------------------------
    # Edit these items
    # -------------------------------------------------------------------

    # Top of the source tree
    # The project directory will be placed under SRCTOP
    SRCTOP=$METABLDDIR

    # -------------------------------------------------------------------
    # Don't edit below here
    # -------------------------------------------------------------------
    # Project ID
    PARENT=pibox
    PRJ=pibox-network-config

    # -------------------------------------------
    # Gitlab config
    # GIT Repo
    export GITREPO=git@gitlab.com:$PARENT/$PRJ.git
	GM_CLONE="git clone $GITREPO $METABLDDIR/$PRJ"

    # Create top level directory, if needed
    mkdir -p $SRCTOP

    # Where I do my dev work
    GM_WORK=$SRCTOP/work
    mkdir -p $GM_WORK

    # Where the SCM is located
    GM_HOME=$SRCTOP/$PRJ
    # Where the source and build directories live
    GM_SRC=$GM_HOME/src
    GM_BUILD=$GM_HOME/bld
    GM_ARCHIVE=$GM_HOME/archive
    GM_PKG=$GM_HOME/pkg
    GM_EXTRAS=$GM_HOME/extras

    # Put the host applications in our path
    # export PATH=$GM_TOOLS:$PATH

    # Make the configured environment available to the build system.
    export GM_WORK
    export GM_ARCHIVE
    export GM_HOME
    export GM_SRC
    export GM_BUILD
    export GM_PKG
    export GM_EXTRAS

    # Some aliases to bounce around directories easily
    alias cdt='cd $SRCTOP'
    alias cdh='cd $GM_HOME'
    alias cdw='cd $GM_WORK'
    alias cdx='cd $GM_SRC'
    alias cdb='cd $GM_BUILD'
    alias cdp='cd $GM_PKG'
    alias cda='cd $GM_ARCHIVE'
    alias cde='cd $GM_EXTRAS'

    # Show the aliases for this configuration
    alias cd?='cdpibox-network-config'
}
function cdpibox-network-config {
echo "
PiBox Network Config Alias settings:
-----------------------------------------------------------------------------
cdt    cd SRCTOP ($SRCTOP)
cdh    cd GM_HOME ($GM_HOME)
cdw    cd GM_WORK ($GM_WORK)
cdx    cd GM_SRC ($GM_SRC)
cdb    cd GM_BUILD ($GM_BUILD)
cda    cd GM_ARCHIVE ($GM_ARCHIVE)

CLONE: $GM_CLONE
"
}
