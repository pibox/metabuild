# These are the opkgs used in PiBox
# This file is used by build.sh to generate the packages.
appmgr ./cross.sh -s ${STAGING} -t ${XID} -o ${OPKG_DIR}
launcher ./cross.sh -s ${STAGING} -t ${XID} -o ${OPKG_DIR}
mjpg-streamer sudo make USEGIT=1 XI=${XID} SD=${STAGING} OPKG=${OPKG_DIR} pkg
omxplayer sudo make XI=${XID} SD=${STAGING} OPKG=${OPKG_DIR} pkg
piboxd ./cross.sh -s ${STAGING} -t ${XID} -o ${OPKG_DIR}
pmsui sudo make XI=${XID} SD=${STAGING} OPKG=${OPKG_DIR} pkg
piboxwww sudo make XI=${XID} SD=${STAGING} OPKG=${OPKG_DIR} pkg
piclock ./cross.sh -s ${STAGING} -t ${XID} -o ${OPKG_DIR}
psplash sudo make XI=${XID} SD=${STAGING} OPKG=${OPKG_DIR} pkg
pipics ./cross.sh -s ${STAGING} -t ${XID} -o ${OPKG_DIR}
videofe ./cross.sh -s ${STAGING} -t ${XID} -o ${OPKG_DIR}
musicfe ./cross.sh -s ${STAGING} -t ${XID} -o ${OPKG_DIR}
picam ./cross.sh -s ${STAGING} -t ${XID} -o ${OPKG_DIR}
pixm ./cross.sh -s ${STAGING} -t ${XID} -o ${OPKG_DIR}
pinet ./cross.sh -S piboxmedia -s ${STAGING} -t ${XID} -o ${OPKG_DIR}
ironman+www sudo make -j4 XI=${XID} SD=${STAGING} OPKG=${OPKG_DIR} HW=media pkg 
piconf sudo make [MODE] XI=${XID} SD=${STAGING} OPKG=${OPKG_DIR} pkg
#
# These are now included in the dev platform
# pibox-network-config ./cross.sh -s ${STAGING} -t ${XID} -o ${OPKG_DIR}
# libpibox ./cross.sh -s ${STAGING} -t ${XID} -o ${OPKG_DIR}
# 
# Not used currently.
# bluez sudo make XI=${XID} SD=${STAGING} OPKG=${OPKG_DIR} B5=1 pkg
# 
# No longer used
# crtmpserver make SD=${STAGING} pkg
# hawkeye sudo make XI=${XID} SD=${STAGING} OPKG=${OPKG_DIR} pkg
# monkey sudo make XI=${XID} SD=${STAGING} OPKG=${OPKG_DIR} pkg
# 
# VideoLib doesn't build easily with this script.
# videolib ant rpm
#
# Metabuild doesn't get distributed.  It's just a build tool.
# metabuild
