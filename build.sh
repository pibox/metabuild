#!/bin/bash -p
#
# PiBox opkg builder script.
# This script downloads and builds a collection
# of opkg's for the PiBox Media Server.
# -----------------------------------------------
shopt -s expand_aliases

# -----------------------------------------------
# Initialization
# -----------------------------------------------
METAPKGDIR=`pwd`/../pkg
METABLDDIR=`pwd`/../bld
PKGLIST=src/pkglist.txt
REPOLIST=src/repolist.txt
MODE="media"
ENV=src/env
SPACES="                                                  "
SHOWCFG=0
CLEAN=0
NOCLEAN=0
CLOBBER=0
DRYRUN=
APPS=
TAG=
VERSION=
DOTAG=0
DOVERSION=0
LIST=0
STAGING=${STAGING:-}
XID=${XID:-/opt/rpiTC}
OPKG_DIR=${OPKG_DIR:-/usr/local/bin}
PKG_SUFFIX="opk"

# Regular colors (add 10 for reverse video)
# 1. Red 2. Green 3. Yellow 4. Blue 5. Purple 6. Cyan 7. Gray
# 8. Underline 9. White 10. White
MSG1="[31m"
MSG2="[32m"
MSG3="[33m"
MSG4="[34m"
MSG5="[35m"
MSG6="[36m"
MSG7="[37m"
MSG8="[38m"
MSG9="[39m"
MSG10="[40m"
MSG11="[41m"
MSG12="[42m"
MSG13="[43m"
MSG14="[44m"
MSG15="[45m"
MSG16="[46m"
MSG17="[47m"
MSG18="[48m"
MSG19="[49m"
EMSG="[0m"

#--------------------------------------------------------------
# Functions
#--------------------------------------------------------------
# Provide command line usage assistance
function doHelp
{
    echo ""
    echo "$0 [-cdklnS | -v vers | -t tag | -s dir | -x dir | -o dir | -a app[:app:...] ]"
    echo "where"
    echo "-c          Remove (re: clean) build artifacts, except opkgs"
    echo "-k          Remove (re: clobber) build artifacts, including opkgs"
    echo "-d          Dry run - show configured env variables"
    echo "-l          List apps that will be built."
    echo "-m mode     Metabuild mode."
    echo "            One of: media, player, kiosk, autokiosk, xeon, desktop, pisentry, pistore, ironman"
    echo "            Default: ${MODE}"
    echo "-n          Don't cleanup build trees"
    echo "-S          Show configuration that will be used with an app build."
    echo "-t tag      Tag repo(s) with the specified GIT tag."
    echo "-v vers     Set version.txt with specified version in all repos."
    echo "-s dir      Staging directory, aka STAGING (rootfs for cross compiling)."
    echo "            Default: ${STAGING}"
    echo "-x dir      Toolchain directory, aka XID."
    echo "            Default: ${XID}"
    echo "-o dir      Where opkg-build can be found, aka OPKG_DIR."
    echo "            Default: ${OPKG_DIR}"
    echo "-a apps     Build only the specified apps"
    echo ""
}

#Support multiple bin/env* directories
function load
{
    for dir in ${ENV}*
    do
        for file in `ls -1 $dir`
        do
            . $dir/$file
        done
    done
}

# Tokenize a pkglist line into components needed for the build
# Each line is of the following format:
# func cmd...
# This function sets the following global variables
# 1. func
# 2. proj
# 3. cmd
# 3. buildCmd
function tokenize
{
    line=$1
    tokens=( $line )

    # We need the function and it's project, if any.
    prefix="${tokens[0]}"
    func="${tokens[0]%\+*}"
    proj="${tokens[0]/${func}/}"
    proj="${proj/+/}"

    remains=( ${tokens[@]/${prefix}/} )
    cmd=${remains[@]}
    buildCmd=`eval echo ${cmd}`
}

# Some build commands might have tokens based on the
# options set by this script.  Parse them here.
function buildCmdTokenHandler
{
    SYSTEM="SYSTEM=${MODE}"
    buildCmd="${buildCmd/\[MODE]/${SYSTEM}}"
}

# Load function scripts and create working directories
function setup
{
    # Load function scripts
    load

    # Create working directories
    mkdir -p $METAPKGDIR
    mkdir -p $METABLDDIR

    # Generate the configuration file that describes how
    # we were run.
    > $METAPKGDIR/config.log
    metalog "Toolchain   : $XID" 
    metalog "Staging tree: $STAGING"
    metalog "PKGLIST     : $PKGLIST"
    metalog "-----------------------------------"
    metalog "Build Commands"
    metalog "-----------------------------------"
}

# Log something to config.log
function metalog
{
    echo "$1" >> $METAPKGDIR/config.log
}

# -----------------------------------------------
# Aliases don't work inside shell script so we
# convert them to functions
# -----------------------------------------------
function cd?
{
    help="cd$func"
    $help
}
function cdt
{
    cd $SRCTOP
}
function cdh
{
    cd $GM_HOME
}
function cdw
{
    cd $GM_WORK
}
function cdx
{
    cd $GM_SRC
    # dir=`pwd`
    # echo "    cdx    : $dir"
}
function cdb
{
    cd $GM_BUILD
    # dir=`pwd`
    # echo "    cdb    : $dir"
}
function cda
{
    cd $GM_ARCHIVE
}
function cdp
{
    if [[ "$GM_PKG" != "" ]]
    then
        if [[ -d $GM_PKG ]]
        then
            cd $GM_PKG
            dir=`pwd`
        else
            dir="Doesn't exist: $GM_PKG"
        fi
    else
        dir="Unknown"
    fi
    # echo "    cdp    : $dir"
}
function cde
{
    cd $GM_EXTRAS
}
function clone
{
    # echo "    clone  : $GM_CLONE"
    $GM_CLONE
}
function build
{
    cdt
    if [[ "${proj}" != "" ]]
    then
        mkdir -p ${proj}
    else
        mkdir -p ${func}
    fi
    cdh
    clone
    echo "  buildCmd: ${buildCmd}"
    echo "  CWD: $(pwd)"
    if [[ $SHOWCFG -eq 1 ]]
    then
        showcfg=`echo $buildCmd | sed 's/pkg/showconfig/'`
        $showcfg
        exit 0
    else
        $buildCmd
    fi
    if [[ $? -ne 0 ]]
    then
        echo "Build failed for $func"
        exit 1
    fi

    # If requested, don't do the cleanup.
    if [[ $NOCLEAN -eq 1 ]]
    then
        return
    fi

    if [[ ! -d $GM_PKG ]]
    then
        echo "Can't find package directory: $GM_PKG"
        exit 1
    fi
    cdp
    cp *.${PKG_SUFFIX} $METAPKGDIR
    metalog "$func: $buildCmd"
    cd $METABLDDIR
    sudo rm -rf *
}

# Version the repos only.
# This will clone the repos and update the version.txt.
function versionRepos
{
    while read line
    do
        comment=`echo $line | cut -c1`
        if [[ "$comment" == "#" ]]
        then
            continue
        fi

        tokenize "${line}"

        # If the repo command is not in the list of requested apps, skip it.
        if [[ -n "${APPS}" ]]; then
            if [[ -n "${proj}" ]]; then
                # echo "APPS: ${APPS}   | func: ${func}   | proj: ${proj}"
                if [[ ! "${APPS}" == "${func}+${proj}" ]]; then
                    # echo "skipping on func+proj"
                    continue
                fi
            else
                # echo "APPS: ${APPS}   | func: ${func}"
                if [[ ! "${APPS}" =~ "${func}" ]]; then
                    # echo "skipping on func"
                    continue
                fi
            fi
        fi

        echo " =================================================================== "
        echo "Func: ${func}"
        echo "Proj: ${proj}"

        # Load the repo function
        ${func} ${proj}

        if [[ $SHOWCFG -eq 1 ]]
        then
            cd?
            continue
        fi

        # Setup and clone of source
        cdt
        [[ -z "${proj}" ]] && mkdir -p $func
        [[ -n "${proj}" ]] && mkdir -p $proj
        cdh
        clone

        # Update the version.txt file
        echo "$VERSION" > version.txt
        if [[ -z "$DRYRUN" ]]; then
            git commit -q -m "Bump to version $VERSION" version.txt

            # Push it upstream
            git push
        else
            echo -n "Updated version: $(cat version.txt)"
            echo ""
        fi

        # If requested, don't do the cleanup.
        if [[ $NOCLEAN -eq 0 ]]
        then
            # Clean up
            cdt
            [[ -z "${proj}" ]] && rm -rf $func
            [[ -n "${proj}" ]] && rm -rf $proj
        fi

    done < $REPOLIST
}

# Tag the repos only.
# This will clone the repos and then apply
# a tag, commit and push upstream.
function tagRepos
{
    # Create the tag
    ECHO=""
    if [ -n "$DRYRUN" ]; then
        ECHO="echo"
    fi

    while read line
    do
        comment=`echo $line | cut -c1`
        if [[ "$comment" == "#" ]]
        then
            continue
        fi

        tokenize "${line}"

        # If the repo command is not in the list of requested apps, skip it.
        if [[ -n "${APPS}" ]]; then
            if [[ -n "${proj}" ]]; then
                # echo "APPS: ${APPS}   | func: ${func}   | proj: ${proj}"
                if [[ ! "${APPS}" == "${func}+${proj}" ]]; then
                    # echo "skipping on func+proj"
                    continue
                fi
            else
                # echo "APPS: ${APPS}   | func: ${func}"
                if [[ ! "${APPS}" =~ "${func}" ]]; then
                    # echo "skipping on func"
                    continue
                fi
            fi
        fi

        echo " =================================================================== "
        echo "Func: ${func}"
        echo "Proj: ${proj}"

        # Load the repo function
        ${func} ${proj}

        # Setup and clone of source
        cdt
        [[ -z "${proj}" ]] && mkdir -p $func
        [[ -n "${proj}" ]] && mkdir -p $proj
        cdh
        clone

        # Tag it.
        $ECHO git tag $TAG

        # Push it upstream
        $ECHO git push origin --tags

        # If requested, don't do the cleanup.
        if [[ $NOCLEAN -eq 0 ]]
        then
            # Clean up
            cdt
            [[ -z "${proj}" ]] && rm -rf $func
            [[ -n "${proj}" ]] && rm -rf $proj
        fi

    done < $REPOLIST
}

#--------------------------------------------------------------
# Read command line arguments
#--------------------------------------------------------------
while getopts ":cdklnSa:m:o:s:t:v:x:" Option
do
    case $Option in
    a) APPS=$OPTARG;;
    x) XID=$OPTARG;;
    s) STAGING=$OPTARG;;
    m) MODE=$OPTARG;;
    o) OPKG_DIR=$OPTARG;;
    t) TAG=$OPTARG
       DOTAG=1
       ;;
    v) VERSION=$OPTARG
       DOVERSION=1
       ;;
    c) CLEAN=1;;
    n) NOCLEAN=1;;
    d) DRYRUN=1;;
    k) CLEAN=1; CLOBBER=1;;
    l) LIST=1;;
    S) SHOWCFG=1;;
    *) doHelp; exit 0;;
    esac
done

# Clean / Clobber handling
if [[ $CLEAN -eq 1 ]]
then
    echo "Cleaning build artifacts"
    sudo rm -rf $METABLDDIR
    if [[ $CLOBBER -eq 1 ]]
    then
        echo "Cleaning opkgs"
        sudo rm -rf $METAPKGDIR
    fi
    exit 0
fi

# Validate MODE
case $MODE in
    'media')        ;;
    'player')       PKGLIST=src/pkglist.player ;;
    'kiosk')        PKGLIST=src/pkglist.kiosk ;;
    'autokiosk')    PKGLIST=src/pkglist.autokiosk ;;
    'xeon')         PKGLIST=src/pkglist.xeon ;;
    'desktop')      PKGLIST=src/pkglist.desktop ;;
    'pisentry')     PKGLIST=src/pkglist.pisentry ;;
    'pistore')      PKGLIST=src/pkglist.pistore ;;
    'ironman')      PKGLIST=src/pkglist.ironman ;;
    *)              doHelp; exit 1;;
esac

# -----------------------------------------------
# Main processing
# -----------------------------------------------

# If setting the version for trees, just jump to that function now.
if [[ $DOVERSION -eq 1 ]]
then
    # Make sure a version string has been supplied.
    if [[ "$VERSION" == "" ]]
    then
        echo "You must supply a version string"
        exit 1
    fi

    # Check for proper configuration
    if [[ ! -f $REPOLIST ]]
    then
        echo "Missing repo list: $REPOLIST"
        exit 1
    fi
    echo "Updating Repo versions with: $VERSION"
    sleep 2
    setup
    versionRepos
    exit 0
fi

# If tagging trees, just jump to that function now.
if [[ $DOTAG -eq 1 ]]
then
    # Check for proper configuration
    if [[ ! -f $REPOLIST ]]
    then
        echo "Missing repo list: $REPOLIST"
        exit 1
    fi
    echo "Taging Repos with: $TAG"
    sleep 2
    setup
    tagRepos
    exit 0
fi

# Check for proper configuration
if [[ ! -f $PKGLIST ]]
then
    echo "Missing package list: $PKGLIST"
    exit 1
fi

# Verify STAGING directory
if [[ "$STAGING" == "" ]]
then
    echo "STAGING variable is not set."
    exit 1
fi
if [[ ! -d $STAGING ]]
then
    echo "STAGING directory does not exist: $STAGING."
    exit 1
fi
export STAGING

# Verify toolchain directory
if [[ "$XID" == "" ]]
then
    echo "XID (toolchain) variable is not set."
    exit 1
fi
if [[ ! -d $XID ]]
then
    echo "XID (toolchain) directory does not exist: $XID."
    exit 1
fi
export XID

# Load function scripts and create working directories
setup

# -----------------------------------------------
# Build process:
# 1. Slurp the build file
# 2. For each line
# 2.1 parse: function, pkg command, pkg dir
# 2.2 call function
# 2.3 run pkg command
# 2.4 cd pkg dir
# 2.5 copy to build's pkg dir
# -----------------------------------------------
while read line
do
    # Skip comments
    comment=$(echo ${line} | cut -c1)
    if [[ "${comment}" == "#" ]]
    then
        continue
    fi

    # Extract the repo command and build command.
    tokenize "${line}"
    buildCmdTokenHandler

    # If the repo command is not in the list of requested apps, skip it.
    # echo "APPS: ${APPS}   | func: ${func}   | proj: ${proj}"
    if [[ -n "${APPS}" ]]; then
        if [[ -n "${proj}" ]]; then
            if [[ ! "${APPS}" =~ (${proj}) ]]; then
                # echo "skipping on proj"
            continue
            fi
        else
            if [[ ! "${APPS}" =~ "${func}" ]]; then
                # echo "skipping on func"
                continue
            fi
        fi
    fi

    # If requested then just list the repo command.
    if [[ ${LIST} -eq 1 ]]
    then
        echo "App: ${func} ${proj}"
        continue
    fi

    # Run the function that sets up the environment for cloning, building, etc.
    echo "Func: ${func}"
    echo "Proj: ${proj}"
    ${func} ${proj}

    # Either show what would happen or actually do it.
    if [[ "${DRYRUN}" == "1" ]]
    then
        echo "Function  : ${func} ${proj}"
        echo "  Build   : ${cmd}"
        echo "  buildCmd: ${buildCmd}"
        cd?
    else
        build
    fi

    echo " "
done < ${PKGLIST}
