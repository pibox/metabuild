#!/bin/bash
# Generate changelogs for GIT project repositories:
# git log v0.5...v0.6 --pretty=format:'%s'
# Note: this script requires the use of the cdtools functions.
# -------------------------------------------------------------------------------------------
METAPKGDIR=`pwd`/../pkg
METABLDDIR=`pwd`/../bld

ECHO=
ENV=src/env
ERRFILE=${METABLDDIR}/clerr.$$
PKGLIST=src/cllist.txt
START=
TAG=
MSG=
CLEAN=0
CLOBBER=0
APPS=
LIST=0
doDebug=0
CLONEONLY=0

# Load utility and cdtools functions
source functions.sh

# -----------------------------------------------
# Functions
# -----------------------------------------------
function doHelp
{
    echo ""
    echo "Generate a Changelog for a collection of repositories."
    echo ""
    echo "$0 [-cdkl | -m <msg> | -t <tag>] -r <repofile> -s <date>"
    echo "where"
    echo "-r <repofile>    File containing repos to process"
    echo "-s <date>        Since this date in the format YYYY-MM-DD"
    echo "-t <tag>         Tag to apply, such as v0.6, before generating changelog."
    echo "-m <msg>         Message with end tag"
    echo "-a apps          Build only the specified apps"
    echo "-d               Dry run (print but don't perform)"
    echo "-c               Remove (re: clean) build artifacts, except opkgs"
    echo "-k               Remove (re: clobber) build artifacts, including opkgs"
    echo "-l               List apps that will be built."
    echo "-C               Clone only"
    echo ""
    echo "To get the data of the last log, check the pibox repo with this command:"
    echo "git log --tags --simplify-by-decoration --pretty=\"format:%ci %d\""
}

function build 
{
    cdt
    echo "func: ${func} / PRJ=${PRJ}"
    # mkdir -p ${func}
    mkdir -p ${PRJ}
    cdh
    clone
    cdx
    debug "PWD=`pwd`"
    if [ ${CLONEONLY} -eq 1 ]
    then
        return
    fi
    if [ "${TAG}" != "" ]
    then
        ${ECHO} eval ${TAGCMD}
        rcCheck $? "Failed: Tag ${func}" || return
        ${ECHO} eval ${PUSHCMD}
        rcCheck $? "Failed: Push tags for ${func}" || return
    fi
    if [ ${doDebug} -eq 1 ]
    then
        ${ECHO} eval ${CLCMD} 
    else
        eval ${CLCMD} >> ${METAPKGDIR}/Changelog.${func} 2>>${ERRFILE}
    fi
    rcCheck $? "Failed: Changelog gen for ${func}" || return
    cdt
    if [ -f ${ERRFILE} ]
    then
        echo "Errors: "
        echo "---------------------------------------"
        cat ${ERRFILE}
        echo "---------------------------------------"
    fi
    rm -rf ${func} ${ERRFILE}
}

# Some build commands might have tokens based on the
# options set by this script.  Parse them here.
function buildCmdTokenHandler
{
    SYSTEM="SYSTEM=${MODE}"
    buildCmd="${buildCmd/\[MODE]/${SYSTEM}}"
}

#--------------------------------------------------------------
# Read command line arguments
#--------------------------------------------------------------
while getopts ":Ccdkla:m:r:s:t:" Option
do
    case $Option in
    a) APPS=${OPTARG};;
    c) CLEAN=1;;
    C) CLONEONLY=1;;
    k) CLEAN=1; CLOBBER=1;;
    l) LIST=1;;
    d) doDebug=1;;
    r) PKGLIST=${OPTARG};;
    s) START=${OPTARG};;
    t) TAG=${OPTARG};;
    m) MSG=${OPTARG};;
    *) doHelp; exit 0;;
    esac
done

# Clean / Clobber handling
if [ ${CLEAN} -eq 1 ]
then
    echo "Cleaning build artifacts"
    sudo rm -rf ${METABLDDIR}
    if [ ${CLOBBER} -eq 1 ]
    then
        echo "Cleaning opkgs"
        sudo rm -rf ${METAPKGDIR}
    fi
    exit 0
fi

#--------------------------------------------------------------
# Validate configuration
#--------------------------------------------------------------
if [ "${PKGLIST}" = "" ]
then
    echo "MISSING REPOS (-r)"
    doHelp
    exit 0
fi

if [ ! -f ${PKGLIST} ]
then
    echo "No such file: ${PKGLIST}"
    doHelp
    exit 0
fi

if [ ${LIST} -eq 0 ] && [ "${START}" = "" ] 
then
    echo "Missing START date"
    doHelp
    exit 0
fi

if [ "${TAG}" != "" ]
then
    echo "TAG is set"
    if [ "${MSG}" = "" ]
    then
        echo "but missing MSG"
        doHelp
        exit 0
    fi
fi

if [ ${doDebug} -eq 1 ]
then
    ECHO=echo
fi

#--------------------------------------------------------------
# Build command strings
#--------------------------------------------------------------
TAGCMD="git tag -f -a ${TAG} -m\"${MSG}\" >${ERRFILE} 2>&1"
PUSHCMD="git push origin --tags >${ERRFILE} 2>&1"
CLCMD="git log --since=${START} --pretty=format:'%s'"

#--------------------------------------------------------------
# Process repo list
# Format:
# repoFile repo [repo ...]
#--------------------------------------------------------------
# Load function scripts
load

# Create working directories
mkdir -p $METAPKGDIR
mkdir -p $METABLDDIR

while read line
do
    comment=`echo ${line} | cut -c1`
    if [ "${comment}" = "#" ]
    then
        continue
    fi
    tokenize "${line}"
    buildCmdTokenHandler

    if [ "${APPS}" != "" ]
    then
        echo "${APPS}" | grep ${func} 2>&1 >/dev/null
        if [ $? -eq 1 ]
        then
            continue
        fi
    fi
    if [ ${LIST} -eq 1 ]
    then
        echo "App: ${func} ${proj}"
        continue
    fi

    echo "Func: ${func}"
    echo "Proj: ${proj}"
    ${func} ${proj}

    if [[ "${DRYRUN}" == "1" ]]
    then
        echo "Function  : ${func} ${proj}"
        echo "  Build   : ${cmd}"
        echo "  buildCmd: ${buildCmd}"
        cd?
    else
        build
    fi

    echo " "
done < ${PKGLIST}

if [ ${LIST} -eq 1 ]
then
    exit 0
fi

#--------------------------------------------------------------
# Consilidate changelogs
#--------------------------------------------------------------
cd ${METAPKGDIR}
for file in `ls -1 Changelog*`
do
    if [ -s ${file} ]
    then
        repo=`echo ${file} | cut -f2 -d"."`
        hdr="Repository: ${repo}"

        echo "-----------------------------------------------------" >> Changelog
        echo "${hdr}" >> Changelog
        echo "-----------------------------------------------------" >> Changelog
        cat ${file} >> Changelog
        echo "" >> Changelog
    fi
done

echo "Changelog generated: "
ls -1 Changelog

