#!/bin/bash -p
# Utility functions
# -----------------------------------------------------
# Test a return code.  Exit with that code if not 0.
function rcCheck
{
    rc=$1
    shift
    msg=$@
    if [ $rc -ne 0 ]
    then 
        echo "$msg"
        exit $rc
    fi
    return $rc
}

# Enable debug output (aka "dry run")
function debug
{
    if [ $doDebug -eq 1 ]
    then
        echo $*
    fi
}

#Support multiple bin/env* directories
function load 
{
    for dir in ${ENV}*
    do
        for file in `ls -1 $dir`
        do
            . $dir/$file
        done
    done
}

# Tokenize a pkglist line into components needed for the build
# Each line is of the following format:
# func cmd...
function tokenize
{
    line=$1
    tokens=( $line )

    # func=${tokens[0]};
    # remains=( ${tokens[@]/$func/} )
    # cmd=${remains[@]}
    # buildCmd=`eval echo ${cmd}`

    # We need the function and it's project, if any.
    prefix="${tokens[0]}"
    func="${tokens[0]%\+*}"
    proj="${tokens[0]/${func}/}"
    proj="${proj/+/}"

    remains=( ${tokens[@]/${prefix}/} )
    cmd=${remains[@]}
    buildCmd=`eval echo ${cmd}`
}

# -----------------------------------------------
# Aliases don't work inside shell script so we 
# convert them to functions
# -----------------------------------------------
function cd? 
{
    help="cd$func"
    $help
}
function cdt 
{
    cd $SRCTOP
}
function cdh 
{
    cd $GM_HOME
}
function cdw 
{
    cd $GM_WORK
}
function cdx 
{
    if [ -d $GM_SRC ]
    then
        cd $GM_SRC
    else
        echo "Can't find $GM_SRC"
        echo "Working from `pwd`"
    fi
    # dir=`pwd`
    # echo "    cdx    : $dir"
}
function cdb 
{
    cd $GM_BUILD
    # dir=`pwd`
    # echo "    cdb    : $dir"
}
function cda 
{
    cd $GM_ARCHIVE
}
function cdp 
{
    if [ "$GM_PKG" != "" ]
    then
        if [ -d $GM_PKG ]
        then
            cd $GM_PKG
            dir=`pwd`
        else
            dir="Doesn't exist: $GM_PKG"
        fi
    else
        dir="Unknown"
    fi
    # echo "    cdp    : $dir"
}
function cde 
{
    cd $GM_EXTRAS
}
function clone 
{
    # echo "    clone  : $GM_CLONE"
    $GM_CLONE
}
